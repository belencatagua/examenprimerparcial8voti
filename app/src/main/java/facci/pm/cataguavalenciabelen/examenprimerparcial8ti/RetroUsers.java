package facci.pm.cataguavalenciabelen.examenprimerparcial8ti;

import com.google.gson.annotations.SerializedName;


public class RetroUsers {
    //Personalizamos los campos//

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("username")
    private String username;

    @SerializedName("email")
    private String email;

    @SerializedName("phone")
    private String phone;

    @SerializedName("website")
    private String website;

    public RetroUsers(String name, String username, String website, String email, String phone, int id) {
        this.name = name;
        this.username = username;
        this.website = website;
        this.email = email;
        this.phone = phone;
        this.id=id;
    }

    //Recupere los datos usando métodos setter / getter//


    public int getId() {
        return id;
    }
    public void setID(int id) {
        this.id = id;
    }


    public String getUser() {
        return name;
    }
    public void setUser(String name) {
        this.name = name;
    }

    /////////////////////////////////////////////////////////
    public String getUserName() {
        return username;
    }
    public void setUserName(String username) {
        this.username = username;
    }

    /////////////////////////////////////////////////////////
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    /////////////////////////////////////////////////////////
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /////////////////////////////////////////////////////////
    public String getWeb() {
        return website;
    }
    public void setWeb(String website) {
        this.website = website;
    }
}
