package facci.pm.cataguavalenciabelen.examenprimerparcial8ti;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

//Extender la clase RecyclerView.Adapter//
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.CustomViewHolder> {

    public List<RetroUsers> dataList;
    public Integer id;
    private RetroUsers objSeleccionado;

    public MyAdapter(List<RetroUsers> dataList){
        this.dataList = dataList;
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        //Obtener una referencia a las Vistas en nuestro diseño//

        TextView textUser, username, website;

        public final View myView;

        CustomViewHolder(View itemView) {
            super(itemView);
            myView = itemView;

            textUser = myView.findViewById(R.id.user);
            username = myView.findViewById(R.id.userName);
            website = myView.findViewById(R.id.website);
        }
    }

    @Override       //Constructor a RecyclerView.ViewHolder//
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_layout, parent, false);

        return new CustomViewHolder(view);


    }

    @Override    //Establecemos los datos//
    public void onBindViewHolder(CustomViewHolder holder, final int position ) {
        holder.itemView.setId(dataList.get(position).getId());
        holder.textUser.setText(dataList.get(position).getUser());
        holder.username.setText(dataList.get(position).getUserName());
        holder.website.setText(dataList.get(position).getWeb());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //objSeleccionado = new RetroUsers();
                //goes to new activity passing the item name
                Intent intent = new Intent(holder.itemView.getContext(), DetalleUsers.class);
                ArrayList<RetroUsers> lstArrayDatos = listToArrayList(dataList);
                id = v.getId();
                for (int i = 1; i  < lstArrayDatos.size(); i++){
                    if(lstArrayDatos.get(i).getId() == id)
                    {
                        objSeleccionado = lstArrayDatos.get(i);
                        break;
                    }
                }
                intent.putExtra("email", objSeleccionado.getEmail());
                intent.putExtra("user", objSeleccionado.getUser());
                intent.putExtra("username", objSeleccionado.getUserName());
                intent.putExtra("phone", objSeleccionado.getPhone());
                intent.putExtra("website", objSeleccionado.getWeb());
                intent.putExtra("id", String.valueOf(objSeleccionado.getId()));
                //begin activity
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override   //Calcular el recuento de item para RecylerView//
    public int getItemCount() {
        return dataList.size();
    }


    public  static  <T> ArrayList<T> listToArrayList(List<T> list){
        return  list != null? new ArrayList<>(list) : null;
    }
}
