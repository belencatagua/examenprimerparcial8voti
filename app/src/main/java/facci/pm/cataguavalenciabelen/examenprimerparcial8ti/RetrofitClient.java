package facci.pm.cataguavalenciabelen.examenprimerparcial8ti;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static Retrofit retrofit;

    //Definimos la URL//
    private static final String BASE_URL = "https://jsonplaceholder.typicode.com";

    //Creamos la instancia Retrofit//
    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)      //Convertimos//
                    .addConverterFactory(GsonConverterFactory.create())     //Creamos la instancia//
                    .build();
        }
        return retrofit;
    }
}