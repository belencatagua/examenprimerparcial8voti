package facci.pm.cataguavalenciabelen.examenprimerparcial8ti;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DetalleUsers  extends AppCompatActivity {

    private RetroUsers item;


    TextView textUser, username, email, phone, website;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_lista);

        Bundle bundle= getIntent().getExtras();
        String id = getIntent().getExtras().getString("id");
        if (id != null && !id.isEmpty()) {
            textUser = findViewById(R.id.user);
            username = findViewById(R.id.userName);
            email = findViewById(R.id.email);
            phone = findViewById(R.id.phone);
            website = findViewById(R.id.website);


            textUser.setText(bundle.getString("user"));
            username.setText(bundle.getString("username"));
            email.setText(bundle.getString("email"));
            phone.setText(bundle.getString("phone"));
            website.setText(bundle.getString("website"));

        }
    }
}
